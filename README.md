# Android MobilePay



# Supported Android versions

Android SDK API 21 and up

# How to install

#### Manual installation

- Download `MobilePay.aar` file from the repository

- Create `libs` subfolder inside Android project's `app` folder

- Put `MobilePay.aar` binary into the `libs` folder

- Make sure under `build.gradle` file for the project there is a flatDirs entry under repositories:

  ```kotlin
  allprojects {
      repositories {
        	.........
          flatDir {
              dirs 'libs'
          }
      }
  }
  ```

- Add `implementation (name:'MobilePay', ext:'aar')` into your `build.gradle` file of the application

- There are several other dependencies that needs to be added to support `MobilePay`. Here is a complete list:

```kotlin
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version"
    implementation 'androidx.appcompat:appcompat:1.3.0'
    implementation 'androidx.core:core-ktx:1.6.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'

    // GSON
    implementation 'com.google.code.gson:gson:2.8.6'

    // Input dialog
    implementation 'com.afollestad.material-dialogs:input:3.3.0'

    //biometric
    implementation 'androidx.biometric:biometric:1.1.0'

    // Koin
    implementation 'org.koin:koin-core:2.1.5'
    implementation 'org.koin:koin-android:2.1.5'
    implementation 'org.koin:koin-androidx-scope:2.1.5'
    implementation 'org.koin:koin-androidx-viewmodel:2.1.5'
    implementation 'org.koin:koin-android-architecture:0.9.1'

    // Retrofit
    implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'
    implementation "io.reactivex.rxjava3:rxjava:3.0.7"
    implementation "io.reactivex.rxjava3:rxkotlin:3.0.1"
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.8.2'
    implementation 'com.squareup.retrofit2:adapter-rxjava3:2.9.0'
    implementation 'com.squareup.retrofit2:converter-jackson:2.5.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.7.2'

    // Navigation
    implementation "androidx.navigation:navigation-fragment-ktx:2.3.5"
    implementation "androidx.navigation:navigation-ui-ktx:2.3.5"

    // Notifications badge
    implementation 'com.nex3z:notification-badge:1.0.4'

    // QR code
    implementation 'com.google.zxing:core:3.4.0'
    implementation 'com.android.support:multidex:1.0.3'

    // Material Design
    implementation 'com.google.android.material:material:1.3.0'

		// Card.io
    implementation 'io.card:android-sdk:5.5.1'

		// JWT parser
    implementation 'com.auth0.android:jwtdecode:2.0.0'
```

SDK uses the following permissiong:

```kotlin
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.USE_BIOMETRIC" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

# How to use 

## Steps to use the SDK:

* Create an intent to load the SDK initial Activity:

```kotlin
val intent = Intent(this, MobilePayActivity::class.java)
```

* Load the configuration object that holds configuration values for the SDK:

```kotlin
val login = SSOLogin(
            customerId = CLIENT_CUSTOMER_ID,
            integrationKey = INTEGRATION_KEY,
            environment = ENVIRONMENT, 
            email = OPTIONAL_CUSTOMER_EMAIL_OR_NULL,
						OPTIONAL_DEEP_LINK) 
```

- `CLIENT_CUSTOMER_ID` - an ID that would later let Tappit connect their customer data to the third party customer data.

* `INTEGRATION_KEY` - the client representing integration key from Tappit
* ENVIRONMENT - MobilePayEnvironment.Test, MobilePayEnvironment.Staging or MobilePayEnvironment.Production. Defaults to MobilePayEnvironment.Production

- `OPTIONAL_CUSTOMER_EMAIL_OR_NULL` - customer email address if required

* `OPTIONAL_DEEP_LINK` - nullable deep link e.g. "example://rewards" string, that if set - will enable and show an extra button on the home screen of the SDK that on tap would attempt to execute the deep link - will try to open an activity registered to handle the deep link. Refer to the Example document for the deep link setup in an app. 

  In case deep link parameter was passed - there is an optional drawable asset that should be provided named `tmw_deep_link` that would be an image of the exatra button on the home screen.

* Put all the variables created into the intext:

  ```kotlin
  intent.putExtra("login", login);
  ```
  
* Execure the intent to show the SDK activity:

  ```kotlin
  startActivity(intent)
  ```

* For any more information refer to the Example project


## Providing additional integration ids for third party services

SDK allows integrating with different external services:

* Ticketmaster
* Tickets.com
* Paciolan
* AXS
* Ticketsocket
* LoyaltyId
* SkiData

To take advantage of any of those integrations please provide an additional array of `IntegrationData` objects to the `SSOLogin`:

```kotlin
val ticketmasterId = IntegrationData(
            integrationType = 0,
            valueType = 1,
            value = "token",
            isGlobal = true
        )
val login = SSOLogin(
            <....>
						integrationData = listOf(ticketmasterId)) 
```

The following are supported values for each item. This list is going to be updated as development progresses:

1. integrationType: TicketMaster = 0, Tickets.Com = 1, Paciolan = 2 AXS = 3 TicketSocket = 4 LoyaltyId = 5 SkiData = 6
2. valueType: User ID = 0, API TOKEN = 1
3. value: actual value of the id
4. isGlobal: true or false depending on if the value is unique globally on any application or not

## Branding assets

### Branding images override

* `tmw_navigation_logo` - main app logo
* `tmw_deep_link` - if your app require deep inking from the SDK into other place. Will add additional button with this image on home screen. (Use alongside optional initialization parameter to enable this feature)

Provide the images named as described inside application's `drawable` directory. Refer to the Example project for reference.

### Override colors

SDK supports these colors overrides:

-   `tmw_back_indicator` - back button tint color
-   `tmw_biometric_indicator` - biometric image tint color
-    `tmw_navigation_background` - navigation bar background tint color
-   `tmw_onboarding_background_text` - text that is on onboarding background tint color
-   `tmw_onboarding_background` - onboarding background tint color
-   `tmw_onboarding_button_background` - buttons which are on the onboarding background tint color
-   `tmw_onboarding_button_text` - onboarding buttons text tint color
-   `tmw_page_header_text` - headers of the page tint color
-   `tmw_primary_background_text` - primary object background text tint color
-   `tmw_primary_background` - primary object background tint color
-   `tmw_secondary_background_text` - secondary object background text tint color
-   `tmw_secondary_background` - secondary object background tint color
-   `tmw_pay_button_background` - pay button on the main home screen backgroung color
-   `tmw_pay_button_text` - pay button on the main home screen text color
-   `tmw_pay_button_border` - pay button on the main home screen border color
- `tmw_home_button_background` - home button (any other button than pay button) on the main home screen backgroung color
- `tmw_home_button_text` - home button (any other button than pay button) on the main home screen text color
- `tmw_home_button_border` - home button (any other button than pay button) on the main home screen border color
- `tmw_primary_button_background` - any general button following the main home screen, that usually  has a possitive effect, background color
- `tmw_primary_button_text` - any general button following the main home screen, that usually  has a possitive effect, text color
- `tmw_primary_button_border` - any general button following the main home screen, that usually  has a possitive effect, border color
- `tmw_secondary_button_background` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, background color
- `tmw_secondary_button_text` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, text color
- `tmw_secondary_button_border` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, border color
- `tmw_custom_unselected_tip_button_background` - tip percentage button default background color
- `tmw_custom_unselected_tip_button_text` - tip percentage button default text color
- `tmw_custom_unselected_tip_button_border` - tip percentage button default border color
- `tmw_custom_selected_tip_button_background` - selected tip percentage button background color
- `tmw_custom_selected_tip_button_text` - selected tip percentage button text color
- `tmw_custom_selected_tip_button_border` - selected tip percentage button border color
- `tmw_unselected_benefit_button_background` - benefits (like giveaway tokens or giftcards) activation button background color
- `tmw_unselected_benefit_button_text` - benefits (like giveaway tokens or giftcards) activation button text color
- `tmw_unselected_benefit_button_border` - benefits (like giveaway tokens or giftcards) activation button border color
- `tmw_selected_benefit_button_background`  - activated benefits (like giveaway tokens or giftcards) activation button background color
- `tmw_selected_benefit_button_text` - activated benefits (like giveaway tokens or giftcards) activation button text color
- `tmw_selected_benefit_button_border` - activated benefits (like giveaway tokens or giftcards) activation button border color
- `tmw_redeemable_campaign_card_text` - redeemable campaign table row text color
- `tmw_redeemable_campaign_card_background` - redeemable campaign table row background color
- `tmw_redeemable_campaign_card_border` - redeemable campaign table row border color
- `tmw_redeemed_campaign_card_text` - redeemed campaign table row text color
- `tmw_redeemed_campaign_card_background` - redeemed campaign table row background color
- `tmw_redeemed_campaign_card_border` - redeemed campaign table row border color
- `tmw_redeemable_campaign_button_background` - redeemable campaign status button background color
- `tmw_redeemable_campaign_button_text` - redeemable campaign status button text color
- `tmw_redeemable_campaign_button_border` - redeemable campaign status button border color
- `tmw_redeemed_campaign_button_background` - redeemed campaign status button background color
- `tmw_redeemed_campaign_button_text` - redeemed campaign status button text color
- `tmw_redeemed_campaign_button_border` - redeemed campaign status button border color
- `tmw_transaction_card_background` - transactions table row background color
- `tmw_transaction_card_text` - transactions table row text color
- `tmw_transaction_card_border` - transactions table row border color
- `tmw_notification_badge_background` - notification badge to indicate updates background color
- `tmw_notification_badge_text` - notification badge to indicate updates text color

To override any combination of those put each color inside the application's `colors.xml` file. Refer to the Example project for more information. Override all the colors as needed.

### Client settings 

All of the client settings (terms and conditions links, help link, brand name etc.) are downloaded from the backend and can be changed through the admin panel.

### Fonts

To override certain fonts, put ttf or otf files with the names listed below inside `font` resource folder of the application to override the existing fonts. Supported fonts in the SDK are:
  - `tmw_bold` - name of the family of the general custom bold font
  - `tmw_normal` - name of the family of the general custom normal font
  - `tmw_benefit_status_button` - benefits (like giveaway tokens or giftcards) status button fonts
  - `tmw_campaign_status_button` - campaign status button font
  - `tmw_custom_tip_button` - custom tip percentage button font
  - `tmw_home_button` - home button (any other button on the home screen rather than pay button) font
  - `tmw_pay_button` - pay button on the home screen font
  - `tmw_primary_button` - any general button after home screen font
  - `tmw_secondary_button` - any 2nd button along side the primary button font

Refer to the Example project setup for a deeper example