package com.example.example

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import com.tappit.mobilepay.signIn.IntegrationData
import com.tappit.mobilepay.signIn.MobilePayActivity
import com.tappit.mobilepay.signIn.MobilePayEnvironment
import com.tappit.mobilepay.signIn.SSOLogin
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeSDK()
//        initializeSDKWithDeepLink()
//        initializeSDKWithIntegrationIds()
    }

    fun initializeSDK() {
        // SET THE INTEGRATION KEY FROM TAPPIT FOR ENVIRONMENT
        val INTEGRATION_KEY = ""

        // Customer id in a third party scope
        val CLIENT_CUSTOMER_ID = ""

        val intent = Intent(this, MobilePayActivity::class.java)
        val login = SSOLogin(
            customerId = CLIENT_CUSTOMER_ID,
            integrationKey = INTEGRATION_KEY,
            environment = MobilePayEnvironment.Test, // environment to target. Need to link with integration key
            email = "example@email.com", //  customer email email
        integrationData = arrayOf())

        intent.putExtra("login", login)
        startActivity(intent)
    }

    fun initializeSDKWithDeepLink() {
        // SET THE INTEGRATION KEY FROM TAPPIT FOR ENVIRONMENT
        val INTEGRATION_KEY = ""

        // Customer id in a third party scope
        val CLIENT_CUSTOMER_ID = ""

        // Optional link to be executed. If passed in - an extra button would appear
        // on the home screen. There needs to be an optional image in app`s drawable directory
        // called tmw_deep_link refer to the integration guide for more details
        // e.g. "example://rewards"
        val OPTIONAL_DEEP_LINK = "example://rewards"

        val intent = Intent(this, MobilePayActivity::class.java)
        val login = SSOLogin(
            customerId = CLIENT_CUSTOMER_ID,
            integrationKey = INTEGRATION_KEY,
            environment = MobilePayEnvironment.Test, // environment to target. Need to link with integration key
            email = "example@email.com", //  customer email
            deepLink = OPTIONAL_DEEP_LINK,// Optional deep link
        integrationData = arrayOf())
        intent.putExtra("login", login);
        startActivity(intent)
    }

    fun initializeSDKWithIntegrationIds() {
        // SET THE INTEGRATION KEY FROM TAPPIT FOR ENVIRONMENT
        val INTEGRATION_KEY = ""

        // Customer id in a third party scope
        val CLIENT_CUSTOMER_ID = ""

        // Optional link to be executed. If passed in - an extra button would appear
        // on the home screen. There needs to be an optional image in app`s drawable directory
        // called tmw_deep_link refer to the integration guide for more details
        // e.g. "example://rewards"
        val OPTIONAL_DEEP_LINK = "example://rewards"

        // Integration ids to connect with third party services
        val ticketmasterId = IntegrationData(
            integrationType = 0,
            valueType = 0,
            value = "ticketmaster_test_id",
            isGlobal = true
        )

        val intent = Intent(this, MobilePayActivity::class.java)
        val login = SSOLogin(
            customerId = CLIENT_CUSTOMER_ID,
            integrationKey = INTEGRATION_KEY,
            environment = MobilePayEnvironment.Test, // environment to target. Need to link with integration key
            email = "example@email.com", // Optional customer email
            deepLink = OPTIONAL_DEEP_LINK,
            integrationData = arrayOf(ticketmasterId)) // Optional third party services integration ids list
        intent.putExtra("login", login);
        startActivity(intent)
    }
}